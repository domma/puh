use serde::{Deserialize, Serialize};
use std::{fmt};

#[derive(Serialize, Deserialize)]
pub struct Book {
    title: String,
    authors: Vec<String>
}

impl fmt::Display for Book {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Book('{}' by {})", self.title, self.authors.join(","))
    }
}
