use tera::{Context, Tera};

use std::io::{Write};
use crate::Book;

pub struct Rendering {
    templates: Tera
}

pub fn new(path: &str) -> Rendering {
    let t = compile_templates!(&format!("{}/**/*", path));
    Rendering{templates: t}
}

impl Rendering {

    pub fn render(self: &Rendering, name: &str, books: &Vec<Book>, writer: &mut Write) -> Result<&str, &str> {
        let mut context = Context::new();
        context.insert("books", &books);
    
        match self.templates.render("index.html", &books) {
            Ok(html) => {
                writer.write(html.as_bytes());
            },
            Err(e) => {
                return Err("error");
            }
        }
        Ok("abc")
    }
}

