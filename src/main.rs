mod book;
mod rendering;

use clap::{App, Arg, crate_description, crate_name, crate_version};
use walkdir::WalkDir;


use std::fs::{File, create_dir_all};
use std::path::Path;
use std::io::{BufReader, BufWriter};
use serde_json::error::Error;

#[macro_use]
extern crate tera;

use book::Book;

fn main() -> Result<(), Error> {

    let app = App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(
            Arg::with_name("data")
                .required(true)
                .help("Path to data files")
        ).arg(
            Arg::with_name("templates")
                .required(true)
                .help("Path to templates files")
            );

    let matches = app.get_matches();

    let data_path = matches.value_of("data").expect("required");
    let template_path = matches.value_of("templates").expect("required");

    let mut vec = Vec::new();

    for entry in WalkDir::new(data_path) {
        if let Ok(e) = entry {
            if e.file_type().is_file() {

                if let Ok(file) = File::open(e.path()) {
                    let reader = BufReader::new(file);

                    let mut books: Vec<Book> = serde_json::from_reader(reader)?;
                    vec.append(&mut books);
                }
            };
        };
    };

    let r = rendering::new(&format!("{}/**/*", template_path));

    create_dir_all("./public");
    if let Ok(f) = File::create(Path::new("./public/index.html")) {
        r.render("index.html", &vec, &mut BufWriter::new(f));
        //out.write(html.as_bytes());
    }

    Ok(())
}
